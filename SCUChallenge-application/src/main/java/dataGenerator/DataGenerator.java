package dataGenerator;

import java.util.ArrayList;
import java.util.Random;

import databaseDriver.DriverDatabaseNeo4j;

public class DataGenerator {
	public static void main(String[] args) throws InterruptedException {
		DriverDatabaseNeo4j db = new DriverDatabaseNeo4j("bolt://localhost:7687", "neo4j", "assd");

		db.openConnection();
		// ArrayList<Intersection> coords=db.getThresholdCriticalNodes(242187500);

		Random random = new Random();
		double min = 0;
		double max = 8585.361328125;
		// double result= min+(random.nextDouble()*(max-min));

		ArrayList<Long> osmids = db.getIntersectionOsmids();

		while (true) {
			for (int i = 0; i < osmids.size(); i++) {
				Long id = osmids.get(i);
				db.setBetweennessIntersection(id, random.nextDouble() * 2000000000);
				if ((i + 1) % 100 == 0) {
					db.setLastModified();
					System.out.println("setLastModified");
				}
			}
			db.setLastModified();
		}
	}
}
