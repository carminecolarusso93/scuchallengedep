package kafka;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import webSocket.WebSocketServer;

public class ConsumerKafka {

	private final static String TOPIC = "test";
	private final static String BOOTSTRAP_SERVER = "localhost:9092";
	private WebSocketServer ws;
	private boolean running = false;
	Thread t;

	public ConsumerKafka(WebSocketServer ws) {
		this.ws = ws;
		t = new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.println("thead start");
				final Consumer<String, String> consumer = createConsumer();
				String value;
				String[] split;
				LocalDateTime time;
				running = true;

				while (running) {
					final ConsumerRecords<String, String> consumerRecords = consumer.poll(Duration.ofSeconds(10));
					System.out.println("timeout");
					if (consumerRecords.count() != 0) {
						
						for (ConsumerRecord<String, String> r : consumerRecords) {
							value = r.value();

							value = value.replace('"', (char) 0);
							value = value.replace('}', (char) 0);

							split = value.split(":", 2);
							value = split[1];
							value = value.trim();
							System.out.println(value);

							time = LocalDateTime.parse((CharSequence) value);
							ws.trigger(time);
						}

					}
					consumer.commitAsync();
				}
				System.out.println("Consumer stopped");
			}
		});
	}

	private static Consumer<String, String> createConsumer() {

		final Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVER);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "KafkaExampleConsumer");
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

		// Create the consumer using props.
		final Consumer<String, String> consumer = new KafkaConsumer<>(props);

		// Subscribe to the topic.
		consumer.subscribe(Collections.singletonList(TOPIC));
		return consumer;
	}

	public void runConsumer() throws InterruptedException {
		t.start();
	}

	public void stop() {
		t.interrupt();
		this.running = false;

	}

	public boolean isRunning() {
		return this.running;
	}
}
