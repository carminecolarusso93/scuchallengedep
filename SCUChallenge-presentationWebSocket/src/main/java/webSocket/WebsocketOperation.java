package webSocket;

public class WebsocketOperation {
	public static final int TOP_CRITICAL_NODES = 0;
	public static final int THRESHOLD_CRITICAL_NODES = 1;
	public static final int NODES_FLOW = 2;
	public static final int SHORTEST_PATH = 3;
	public static final int GET_INTERSECTION = 4;
	public static final int GET_STREET = 5;
	public static final int ERROR = -1;
	
	public static final int MSG = 6;
	
	private int operation;
	private Object paramater;
	
	public WebsocketOperation(int operation, Object paramater) {
		super();
		this.operation = operation;
		this.paramater = paramater;
	}

	public int getOperation() {
		return operation;
	}

	public void setOperation(int operation) {
		this.operation = operation;
	}

	public Object getParamater() {
		return paramater;
	}

	public void setParamater(Object paramater) {
		this.paramater = paramater;
	}

	@Override
	public String toString() {
		return "WebsocketOperation [operation=" + operation + ", paramater=" + paramater + "]";
	}
	
	
	
}
