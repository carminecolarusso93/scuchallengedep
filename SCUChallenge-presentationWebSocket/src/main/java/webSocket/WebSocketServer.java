package webSocket;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;

import dataModel.Coordinate;
import dataModel.Intersection;
import databaseDriver.DriverDatabase;
import databaseDriver.DriverDatabaseNeo4j;
import databaseManagementService.DatabaseManagementService;
import kafka.ConsumerKafka;

@ServerEndpoint(value = "/websocket/serverEndPoint")
public class WebSocketServer {

//	@Resource
//	private TimerService timerService;
//
//	private Timer timer;
//	private Thread timer;
	private DriverDatabase database;
	private Session session;
	private WebsocketOperation wsOp;
	private Gson gson;
	private LocalDateTime lastModified;
	private Logger logger = Logger.getLogger(WebSocketServer.class);
	private ConsumerKafka ck;

	@OnOpen
	public void start(Session session) throws IOException {
		logger.info("Connection opened: " + session.getId());
		this.session = session;
		initConversation();
	}

	@OnMessage
	public void onMessage(Session session, String message) throws IOException, ParseException, InterruptedException {
		logger.info("Ricevuto messaggio da: " + session.getId());

		setWebSocketOperation(message);
		processWebSocketOperation(session, wsOp);

		if (!ck.isRunning()) {
			ck.runConsumer();
			logger.info("consumer starting");
		}
	}

	@OnClose
	public void onClose(Session session) {
		logger.info("Session Closed: " + session.getId());
		try {
			ck.stop();
		} catch (NullPointerException e) {
			logger.warn("No timer set");
		}
	}

	private void setWebSocketOperation(String message) throws ParseException {
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(message);

		int operation = Integer.parseInt(json.get("op") + "");

		switch (operation) {

		case WebsocketOperation.TOP_CRITICAL_NODES:

			int topCN = Integer.parseInt(json.get("topCN") + "");
			wsOp = new WebsocketOperation(operation, topCN);

			break;

		case WebsocketOperation.THRESHOLD_CRITICAL_NODES:

			Double treshCNS = Double.parseDouble(json.get("treshCN") + "");
			wsOp = new WebsocketOperation(operation, treshCNS);

			break;

		case WebsocketOperation.NODES_FLOW:
			Long nf = Long.parseLong(json.get("nf") + "");
			wsOp = new WebsocketOperation(operation, nf);

			break;

		case WebsocketOperation.SHORTEST_PATH:
			Long sn = Long.parseLong(json.get("sn") + "");
			Long dn = Long.parseLong(json.get("dn") + "");

			ArrayList<Long> parameter = new ArrayList<>();
			parameter.add(sn);
			parameter.add(dn);
			wsOp = new WebsocketOperation(operation, parameter);

			break;

		default:
			wsOp = new WebsocketOperation(WebsocketOperation.ERROR, null);

		}
	}

	private void processWebSocketOperation(Session session, WebsocketOperation wsOp) {
		JSONObject jsonResp = new JSONObject();

		logger.info("sessionID: " + session.getId() + " wsOP: " + wsOp);
		switch (wsOp.getOperation()) {
		case WebsocketOperation.TOP_CRITICAL_NODES:

			if (wsOp.getParamater() instanceof Integer) {
				Integer topCN = (Integer) wsOp.getParamater();

				System.out.println("database: " + database);
				System.out.println("top: " + topCN);
				ArrayList<Intersection> respTop = database.getTopCriticalNodes(topCN);

				jsonResp.put("op", WebsocketOperation.TOP_CRITICAL_NODES);
				jsonResp.put("mex", gson.toJson(respTop));
			} else {
				jsonResp.put("op", WebsocketOperation.ERROR);
			}
			break;

		case WebsocketOperation.THRESHOLD_CRITICAL_NODES:

			if (wsOp.getParamater() instanceof Double) {
				Double treshCN = (Double) wsOp.getParamater();
				ArrayList<Intersection> respTresh = database.getThresholdCriticalNodes(treshCN);

				jsonResp.put("op", WebsocketOperation.THRESHOLD_CRITICAL_NODES);
				jsonResp.put("mex", gson.toJson(respTresh));
			} else {
				jsonResp.put("op", WebsocketOperation.ERROR);

			}
			break;

		case WebsocketOperation.NODES_FLOW:
			if (wsOp.getParamater() instanceof Long) {
				Long nf = (Long) wsOp.getParamater();

				Intersection nodeF = database.getIntersectionLight(nf);

				jsonResp.put("op", WebsocketOperation.NODES_FLOW);
				jsonResp.put("mex", gson.toJson(nodeF));
			} else {
				jsonResp.put("op", WebsocketOperation.ERROR);
			}
			break;

		case WebsocketOperation.SHORTEST_PATH:

			if (wsOp.getParamater() instanceof ArrayList<?>) {

				ArrayList<Long> parameter = (ArrayList<Long>) wsOp.getParamater();
				ArrayList<Coordinate> spCoords = database.shortestPathCoordinate(parameter.get(0), parameter.get(1));

				jsonResp.put("op", WebsocketOperation.SHORTEST_PATH);
				jsonResp.put("mex", gson.toJson(spCoords));
			} else {
				jsonResp.put("op", WebsocketOperation.ERROR);
			}

			break;

		default:
			jsonResp = new JSONObject();
			jsonResp.put("op", WebsocketOperation.ERROR);
		}

		try {
			session.getBasicRemote().sendText(jsonResp.toJSONString());
		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	private void initConversation() {

		wsOp = null;
		gson = new Gson();
		String uri = "bolt://" + DatabaseManagementService.DATABASE_IP + ":" + DatabaseManagementService.BOLT_PORT;
		database = new DriverDatabaseNeo4j(uri, DatabaseManagementService.DEFAULT_USERNAME,
				DatabaseManagementService.DEFAULT_PASSWORD);
		database.openConnection();

		lastModified = database.getLastModified();
//		timer = new Thread(new Runnable() {
//			@Override
//			public void run() {
//
//				lastModified = database.getLastModified();
//				try {
//					while (true) {
//						//LocalDateTime databaseLastModified = database.getLastModified();
//						if (wsOp != null && lastModified.isBefore(databaseLastModified)) {
//							logger.info("Update: " + session.getId());
//							processWebSocketOperation(session, wsOp);
//							lastModified = database.getLastModified();
//						}
//						Thread.sleep(10000);
//						System.out.println("timeout: " + session.getId());
//					}
//				} catch (InterruptedException e) {
//					logger.warn("Thread timer interrupted: " + session.getId());
//					// e.printStackTrace();
//				}
//			}
//		});
		ck = new ConsumerKafka(this);
	}

	public void trigger(LocalDateTime time) {
		if (time.isAfter(lastModified)) {
			processWebSocketOperation(session, wsOp);
			lastModified=time;
		}
	}
}
